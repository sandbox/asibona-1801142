<?php

define('zip_image_import_SELECT', t('Seleziona'));

/** 
 * function list_type() supports select list options [node_type]
 * @return  list containing available content type names 
 */
function list_type(){

	$query = db_select('node_type','node');
	$query->fields('node',array('name'));
	$query->fields('node',array('type'));
	$result=$query->execute();

	$list	= array(zip_image_import_SELECT => zip_image_import_SELECT);
	foreach ($result as $row) {
		//array_push($list, $row->name);
		$list[$row->type]=$row->name;
	}
	return $list;
}

/**
 * function list_field($type) supports select list options [node_field]
 * @param  $type name of referring content type			
 * @return list containing image field names ,within content $type
 */
function list_field($type){

	$query= db_select('field_config_instance','fi');
	$query->join('field_config','fc', 'fi.field_id = fc.id');
	$query->condition('fi.bundle',$type,'=');	
	$query->condition('fc.type','image','=');		
	$query->fields('fi',array('field_id'));
	$query->fields('fc',array('field_name'));

	$list=array(zip_image_import_SELECT => zip_image_import_SELECT );

	$result=$query->execute();	
	foreach ($result as $row) {
		$list[$row->field_name]=$row->field_name;
	}
	
	return $list;
}

/**
 * function list_option($type) supports radio buttons [option]
 * @param  $type  name of referring content type	
 * @return list containing image field names ,within content type $type
 */
function list_option($type){

	$list=array('nid' => 'node id' , 'title' => 'node title');

	$query= db_select('field_config_instance','fi');
	$query->join('field_config','fc', 'fi.field_id = fc.id');
	$query->condition('fi.bundle',$type,'=');	
	$or=db_or()->condition('fc.type','text')->condition('fc.type','number_integer');
	$query->condition($or);	
	$query->fields('fi',array('field_id'));
	$query->fields('fc',array('field_name'));
	$query->fields('fc',array('type'));
	$result=$query->execute();

	foreach ($result as $row) {
		$list[''.$row->field_name.''] = $row->field_name.' ('.$row->type.')';
	}

	return $list;

}
 
/**
 * fmodule configuration form  admin/config/media/image_product
 * variables saved into database
 * image_import_type   = name of selected content type 
 * image_import_field  = name of image field upon which images are uploaded
 * image_import_id_dir = name of text field or numer_integer field used to identify node univocally
 */

function zip_image_import_admin($form, &$form_state){
	
/**	
 * @var $type_name  name of selected content type [node_type]
 * @var $field_name name of selected field [node_field]
 * @var $id_dir    radio buttons value [option]
 * @var $form_state['type'] variable changing at select change event [node_type]
 */

	
	$type_name  = variable_get('image_import_type',   -1 );
	$field_name = variable_get('image_import_field',   -1 );
	$id_dir     = variable_get('image_import_id_dir', 'nid' );
		


	if( empty( $form_state['input']['node_type'] ) ){ 

		$form_state['type'] = "";
	
		if( $type_name !=-1 ){
			$form_state['type']  = $type_name;
		
			$option_image = list_field( $type_name); //lista completa field image
			$option_field = list_option($type_name);

			$form['riassunto']= array(
				'#type'=>'fieldset',
				'#title'=>t('parametri attuali'),
			);

			$form['riassunto']['MARKUP']= array(
			'#markup' => t("type: $type_name <br> field-image: $field_name <br> id-directory: $id_dir"),
			);
		}
	}

	else{ 

		$type_name = $form_state['input']['node_type'];
		$form_state['type']  = $type_name;

		$option_image = list_field( $type_name);
		$option_field = list_option($type_name);

		$field_name  = zip_image_import_SELECT;
		$id_dir      = "nid";
		unset($form_state['input']['node_field']);
		unset($form_state['input']['option']);
		
	}

	/*	
	* select content type
	*/
	$form['item'] = array(
		'#type'=>'fieldset',
		'#title'=>t('list node type'),
		'#collapsible' => FALSE,
		'#collapsed' => TRUE
	);

	$form['item']['node_type']=array(
		'#type' => 'select',
		'#title' => t('select content type'),
		'#default_value' => $type_name,
		'#options' => list_type(),
		'#description' => t('content type upon which images must be uploaded'),
		'#ajax' => array(
			'event' => 'change',
			'callback' => 'item_add_field',  		
			'wrapper' => 'update-wrapper',
			'effect' => 'fade',
			'progress' => array('type' => 'throbber', 
			'message' => t('Please wait.')
			)
		),

	);

	$form['field']= array(
		'#type'=>'fieldset',
		'#title'=>t("list field in node type $type_name"),
		'#prefix' => '<div id="update-wrapper">',
  		'#suffix' => '</div>',
	);

	if(!empty($form_state['type']) && strcmp($type_name,zip_image_import_SELECT) ){

	
		/*
		* select field image name in content type 
		*/
		$form['field']['node_field']=array(
			'#type' => 'select',
			'#title' => t('select field image to update'),
			'#options' => $option_image,
			'#default_value' =>  $field_name,
		);
		/*
		* scelta radios field name di tipo text e number in content type 
		*/
		$form['field']['option'] = array(
			'#type' => 'radios', 
		  	'#title' => t('select fieled'), 
			'#default_value' =>$id_dir ,
			'#options' => $option_field,
			'#description' => t('univocal field to be used as directory name'),
		);

	
	
	}

	$form['submit'] = array(
		'#type'  => 'submit',
		'#value' => 'update'
	);
	return $form;
}



function item_add_field($form, &$form_state) {
	return $form['field'];
}


function zip_image_import_admin_validate($form, &$form_state) {
  $type   = $form_state['values']['node_type'];
  $field  = $form_state['values']['node_field'];

  if( !strcmp($type , zip_image_import_SELECT ) ){  form_set_error('node_type', t('you must select a content type') ); }
  if( !strcmp($field, zip_image_import_SELECT ) ){  form_set_error('node_field',t('you must select an image field'));}

}


function zip_image_import_admin_submit($form, &$form_state){

	$type_name   = $form_state['input']['node_type'];
	$field_name  = $form_state['input']['node_field'];
	$option_dir = $form_state['input']['option'];

 	variable_set('image_import_type' , $type_name);
	variable_set('image_import_field', $field_name);
	variable_set('image_import_id_dir', $option_dir);
}

?>
