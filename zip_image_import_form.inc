<?php
/**
* @var $type_name : 	name of selected content type 
* @var $field_name:	name of image field to manage
* @var $id 	  :	univocal id used to identify a node default nid or additional number or text field
* @var  $dir	  : 	folder path where zip files through ftp or interface.
* @var ZIP_IMAGE_IMPORT_PATH : path to file system config
* @var ZIP_IMAGE_IMPORT_DIR  : temporary path to upload compressed file.
*/


define('ZIP_IMAGE_IMPORT_PATH', DRUPAL_ROOT.'/'.variable_get('file_public_path'));
define('ZIP_IMAGE_IMPORT_DIR','import_image');


function zip_image_import_form($form, &$form_state){

	if( !strcmp(variable_get('file_public_path'),'') ){
		$output = '<a href="'.base_path().'admin/config/media/file-system"> configura il file-system </a>';
		drupal_set_message($output);
		return null;
	}
	
	drupal_add_js(drupal_get_path('module', 'zip_image_import').'/select_all.js');

	$type_name  = variable_get('image_import_type',   -1 );
	$field_name = variable_get('image_import_field',   -1 );
	$id_dir     = variable_get('image_import_id_dir', 'nid' );
	$dir 	    = ZIP_IMAGE_IMPORT_PATH.'/'.ZIP_IMAGE_IMPORT_DIR;

	file_prepare_directory($dir , FILE_CREATE_DIRECTORY );

	if($type_name==-1 || $field_name==-1){
			
		$output = '<a href="'.base_path().'admin/config/media/image_zip">configura il modulo</a>';
		drupal_set_message($output);	
			
		return null;
	}

	/*
	 *  option for list of node in type $type_name 
	 */
	$nodes = node_load_multiple(array(), array("type" => $type_name));

	$options = array();
	foreach ($nodes as $node){ $options[''.$node->$id_dir.''] =  t( '<b>'.$node->title.'</b>' .' id-univoco: '.$node->$id_dir ); }

	 
	/* $form_state['numero'] number file upload */
	if (empty($form_state['numero'])) {$form_state['numero'] = 1;}
	
	$form['#attributes']['enctype'] = 'multipart/form-data';


	/*  form fieldset */
	$form['item']= array(
			'#type'=>'fieldset',
			'#title'=>t('list of node '.$type_name),
			'#collapsible' => TRUE,
			'#collapsed' => TRUE
	);

	$form['item-option']= array(
				'#type'=>'fieldset',
				'#title'=>t('list of zip upload'),
				'#collapsible' => TRUE,
				'#collapsed' => FALSE,
				'#prefix' => '<div id="update-wrapper">',
				'#suffix' => '</div>',
				'#description' => t('name of directories within zip file must be univocal'),
	);

	$form['item-ftpimage']= array(
				'#type'=>'fieldset',
				'#title'=>t('list of FTP zip upload'),
				'#collapsible' => TRUE,
				'#collapsed' => FALSE,
				'#prefix' => '<div id="update-wrapper">',
				'#suffix' => '</div>',
	);


	/* list node type $type_name checkboxes */
	$form['item']['select'] = array(
		'#type' => 'checkbox',
		'#title' => t('select all')
	);
	$form['item']['params'] = array(
		'#type' => 'checkboxes',
		'#title' => t('select nodes to update'),
		'#options' => $options,
	);

	/* field uplaod zip file */
	for ($a = 0; $a < $form_state['numero']; $a++) {
		$form['item-option']['upload'][$a] = array(
			'#title' => t('Attach file'),
			'#type' => 'managed_file',
			'#upload_validators' => array(
				'file_validate_extensions' => array('zip gzip'),
				'file_validate_size' => array(10 * 1048576),
			),
			'#upload_location' => 'public://'.ZIP_IMAGE_IMPORT_DIR,
			'#description'=> t('supported extensions (zip , gzip)')
		);
	}

	$form['item-option']['add'] = array(
	'#type' => 'submit',
	'#value' => t('Attach another file'),
	'#submit' => array('addmore_add_one'), 
		'#ajax' => array(
			'callback' => 'addmore_callback',  		
			'wrapper' => 'update-wrapper',
		),
	);
	/* end uplaod */

 	/*field input ftp*/
 	$form['item-ftpimage']['ftp']=array(
		 '#type' => 'checkbox',
		 '#options' => array(0,1),
     		 '#title' => t('carica file via ftp'),
 	);
	$form['item-ftpimage']['ftpdir']=array(
		 '#type' => 'textfield',
     		 '#title' => t('name directory ftp'),
		 '#size' => 20, 
		 '#description'=> t('directory must be created in '.zip_image_import_PATH.'do not insert /'),
 	);
	/*end ftp*/

 	$form['submit'] = array(
  		'#type'  => 'submit',
   		'#value' => 'update'
  	);

	return $form;

}

function addmore_callback($form, $form_state) {

	return $form['item-option'];
}

function addmore_add_one($form, &$form_state) {

	$form_state['numero']++;
	$form_state['rebuild']=TRUE;
}

function zip_image_import_form_validate($form, &$form_state){

	$ftp 		= $form_state['input']['ftp'];
	$ftp_dir	= $form_state['input']['ftpdir'];

	/*ftp checkbox */
	if(!empty($ftp) && empty($ftp_dir) ){
		 form_set_error('ftpdir', t('must insert a valid directory name'));
	}

	/*seleziona almeno un nodo*/
	$input_node = 0;
	foreach ($form_state['input']['params'] as $node_id ){
		if( isset($form_state['input']['params'][$node_id]) ){
			$input_node=1; break;
		}
	}
	if($input_node==0){ form_set_error('params', t('must select at least one node')); }

	/*inserisci almeno un file*/
	if(empty($ftp) && $form_state['input'][0]['fid'] == 0 ){
 		form_set_error('0', t('must upload at least one file'));
	}
		
}


/* function for Submit form */
function zip_image_import_form_submit($form, &$form_state){

	$ftp 		= $form_state['input']['ftp'];
	$ftp_dir	= $form_state['input']['ftpdir'];
	
	$field_name = variable_get('image_import_field',   -1 );
	$type_name  = variable_get('image_import_type',   -1 );
	$id         = variable_get('image_import_id_dir', 'nid' );
	
	
	if(!$ftp) { 	$dir = ZIP_IMAGE_IMPORT_PATH.'/'.ZIP_IMAGE_IMPORT_DIR; }
	else { 		$dir = ZIP_IMAGE_IMPORT_PATH.'/'.$ftp_dir; }


	/*
	*  recupero le info sul field	
	*  directory use in file_copy()
	*  extension use in in $mask
	*/
	$settings = field_info_instances('node',$type_name);
	$field_settings['directory']  =  $settings[$field_name]['settings']['file_directory'];
	$field_settings['extensions'] =  $settings[$field_name]['settings']['file_extensions'];
	
	/* preparo le directory inserita nei settings del field*/
	if(!empty($field_settings['directory']) ){
		$dir_content=ZIP_IMAGE_IMPORT_PATH.'/'.$field_settings['directory'];
		file_prepare_directory( $dir_content  , FILE_CREATE_DIRECTORY );
	}

	/**
	 *   @see file_scan_directory() folder names within zip file .zip, 
	 *   must have field set from module administration page as name
	 */

  	if( is_dir($dir) ){
		$mask = '(zip|gzip)';
		foreach (file_scan_directory( $dir ,  $mask , array('recurse' => FALSE)) as $file){
			$zip_name=$file->filename;
			$zip = new ZipArchive;
			if( $zip->open($dir.'/'.$zip_name) === TRUE) {
				if(!$zip->extractTo($dir)) { 
						$message='file '.$zip_name.': folder not found';
						drupal_set_message($message, 'error'); 
				}else{
						$message='file '.$zip_name.': extract';
						drupal_set_message($message);
				}
				$zip->close();
			}
		}
	}



	/*
	 *   per ogni nodo selezionato nell'elenco controllo dentro la direcotry $dir se sono presenti immagini
	 */

	foreach ($form_state['input']['params'] as $node_id ){
		
		if( isset($form_state['input']['params'][$node_id]) ){
			 
			 $node = node_load($node_id);
			 $image_field = field_get_items('node', $node, $field_name);
			 $node_dir = $dir.'/'.$node->$id;
	
			 if( is_dir($node_dir) ){
  		 		
        			/*
				 *  create file object for each image within directory $dir
				 *  associate to field [$field_name] and save node
				 *  ksort order by name.
				 */
				$mask = '('.str_replace(" ", "|", $field_settings['extensions']).')';
				$tmp_dir	=	array();
				$tmp_dir	=	file_scan_directory( $node_dir , $mask , array('.', '..', 'CVS'), array('recurse' => FALSE));
				ksort($tmp_dir);

				$i=0;
			 	foreach ($tmp_dir as $file){
						$tmp_file = (object)array(
								"uid" => 1,
								"uri" => $file->uri,
								"filemime" => file_get_mimetype($file->uri),
								"status" => 1
						);
						$tmp_file = file_copy($tmp_file , 'public://'.$field_settings['directory']);
						$image_field['und'][$i] = (array) $tmp_file;
						
						$i++;
						
				}

				$node->$field_name = $image_field;
				node_save($node); 

				$message='node '.$node->title.': '.$i.'  images added';
				drupal_set_message($message);
			}
		}
	}


	/*
	 *     elemino i file .zip, allegati rimuovendo i nomi dal database.	
	 *     elimino la cartella $dir 
	 *     che verrà ricreata all'invio del form.
	 */
	if(is_dir($dir)){
		$mask = '(zip|gzip)';
	 	foreach (file_scan_directory( $dir ,  $mask , array('recurse' => FALSE)) as $file){
				$name = $file->filename;
				$query = db_delete('file_managed');
				$query->condition('filename',$name);
				$query->execute();
		}
	}
	file_unmanaged_delete_recursive($dir);	

}
?>
